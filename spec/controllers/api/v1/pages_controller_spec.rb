require 'spec_helper'

describe API::V1::PagesController, type: :controller do

  describe "GET #parse" do 
    
    context "Parse a valid URL" do
      before(:each) do
        @url = "#{Rails.root.to_s}/spec/factories/test_file.html"
        get 'parse', params: {url: @url}, format: :json
      end

      it { should respond_with 200 }

      it 'returns parsed page info' do
        expect(json['title']).to eq ('LIM Global')
        expect(json['url']).to eq (@url)
      end
      
    end

    context "Parse an invalid URL" do
      before(:each) do
        @url = ""
        get 'parse', params: {url: @url}, format: :json
      end
      it { should respond_with 400 }
    end
  
  end

  describe "GET #index" do

    before(:each) do
      @pages = create_list(:page, 10)
      get 'index', format: :json
    end

    it { should respond_with 200 }

    it "returns all the pages and tags" do
      expect(json['pages'].length).to eq @pages.length
      expect(json['pages'].first['tags'].length).to eq 10
    end  
      
  end  
  
end 

    
 


 
  
  


  
