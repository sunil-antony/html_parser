FactoryGirl.define do
  factory :page do
    title { Faker::Lorem.sentence }
    url { Faker::Internet.url }

    after(:create) do |page, evaluator|
      create_list(:tag, 10, page: page)
    end
  end
end