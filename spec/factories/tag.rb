FactoryGirl.define do
  factory :tag do
    name { ['h1', 'h2', 'h3', 'a'].sample }
    content { Faker::Lorem.sentence }
    page
  end
end