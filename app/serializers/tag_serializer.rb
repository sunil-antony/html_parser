class TagSerializer < ActiveModel::Serializer
  attributes :name, :content
end
