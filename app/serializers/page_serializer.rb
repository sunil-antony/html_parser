class PageSerializer < ActiveModel::Serializer
  attributes :title, :url, :created_at, :tags
end
