class API::V1::PagesController < ApplicationController
  
  
  # GET /api/v1/pages
  def index
    pages = Page.includes(:tags)
    render json: pages, status: 200
  end

  # GET /api/v1/pages/parse
  def parse
    begin 
      parser = HtmlParserService.new(params[:url]) 
      page = parser.parse
    rescue Exception => e
      render json: e.message , status: 400
    else
      render json: page, status: 200, root: false
    end
  end
  

end