class HtmlParserService
  # HTML parse service using 'nokogiri' gem

  require 'open-uri'

  def initialize url
    @url = url
  end

  def parse
    begin
      doc = Nokogiri::HTML(open(@url))
      title = doc.css('title').first.content
      page = Page.create!(url: @url, title: title)
      doc.css('h1, h2, h3, a').each do |tag|
        page.tags.create(name: tag.name, content: tag.content) 
      end  
    rescue Exception => e
      raise e.message
    else
      page
    end
  end

end