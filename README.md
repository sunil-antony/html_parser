* Ruby version: 
* *  2.3.0 , Use Rbenv for version management

* System dependencies: 
* * Rails 5, Rspec, Nokogiri
 
* Database creation: 
* * rake db:migrate

* API Endpoints
* *   GET /api/v1/pages/parse?url=<page url>
      #parse and index the html page on the url specified
        
* *  GET /api/v1/pages
     #get all index pages and tags 
 
*  How to run the test suite
* *    rake db:test:prepare
* *    bundle exec rspec
