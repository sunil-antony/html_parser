Rails.application.routes.draw do

  namespace :api, defaults: {format: 'json'}, as: nil do
    namespace :v1 do
      resources :pages, only: [:index] do
        collection do
          get 'parse' 
        end
      end
    end
  end  
 
end
